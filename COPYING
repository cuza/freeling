
    ---------------------------------------------------------        
    COPYING FreeLing: Open-Source Suite of Language Analyzers
    ---------------------------------------------------------        

  The contents of this package are distributed under different open
licenses.  If you want to redistribute this software, part of it, or
derived works based on it or on any of its parts, make sure you are
doing so under the terms stated in the license applying to each of the
involved modules.

  The licenses applying to the modules and linguistic data contained
in FreeLing are the following:

 1.- English WordNet synset and relation data, contained in files
     data/en/senses30.src and data/common/wn30.src in this package are
     distributed under the original WordNet license. You can find a
     copy in the file LICENSES/WN.license included in this package or
     at http://wordnet.princeton.edu

 2.- Data for Catalan, Galician, and Spanish WordNets in files
     data/(ca|gl|es)/senses30.src are extracted from the MCR3.0
     project (http://adimen.si.ehu.es/web/MCR). They are distributed
     under their original "Attribution Unported" Creative Commons
     license. You'll find the license in the file
     LICENSES/CC-BY.license included in this package or at
     http://creativecommons.org/licenses/by/3.0

 3.- Data for Italian WordNet in file data/it/senses30.src are
     extracted from OpenWN project MultiWordnet project developed by
     Fondazione Bruno Kessler (http://multiwordnet.fbk.eu). It is
     distributed under its original Creative Commons Attribution 3.0
     Unported License.  You'll find the license in the file
     LICENSES/CC-BY.license included in this package or at
     http://creativecommons.org/licenses/by/3.0

 4.- Data for Portuguese WordNet in file data/pt/senses30.src are
     extracted from brazilian-wn project
     (https://github.com/arademaker/wordnet-br).  It is distributed
     under its original Creative Commons Attribution ShareAlike 3.0
     License.  You'll find the license in the file
     LICENSES/CC-BY-SA.license included in this package or at
     http://creativecommons.org/licenses/by-sa/3.0

 5.- Data for Slovenian WordNet in file data/sl/senses30.src is 
     extracted from sloWNet project (http://lojze.lugos.si/darja/slownet.html)
     developed at Jozef Stefan Institute (http://www.ijs.si). 
     It is distributed under its original Creative Commons 
     Attribution-Noncommercial-ShareAlike 3.0 License.  
     You'll find the license in the file
     LICENSES/CC-BY-NC-SA.license included in this package or at
     http://creativecommons.org/licenses/by-nc-sa/3.0

 6.- Links from WordNet synsets to Cyc concepts (http://www.cyc.com)
     have been obtained from OpenCyc OWL files, and are distributed
     under their original "Attribution Unported" Creative Commons
     license. You'll find the license in the file
     LICENSES/CC-BY.license included in this package or at
     http://creativecommons.org/licenses/by/3.0
 
 7.- Italian dictionary contained in the files in data/it/dictionary
     (this package) and share/it/dicc.src (after installation) is
     straightforwardly extracted from Morph-it! lexicon developed at
     the University of bologna (see
     http://sslmitdev-online.sslmit.unibo.it/linguistics/morph-it.php),
     and is distributed under an "Attribution-ShareAlike" Creative
     Commons license.  You'll find the license in the file
     LICENSES/CC-BY-SA.license included in this package or at
     http://creativecommons.org/licenses/by-sa/3.0/

 8.- Galician dictionary contained in the files in data/gl/dictionary
     (this package) and share/gl/dicc.src (after installation) is
     straightforwardly extracted from the OpenTrad project
     (http://www.opentrad.org) and has been developed by the Seminario
     de Ling��stica Inform�tica (http://sli.uvigo.es) at Universidade
     de Vigo, and extended by Pablo Gamallo and Marcos Garcia, from
     Universidade de Santiago de Compostela.  It is distributed under
     an "Attribution-ShareAlike" Creative Commons license. You'll find
     the license in the file LICENSES/CC-BY-SA.license included in
     this package or at http://creativecommons.org/licenses/by-sa/3.0/

 9.- Spanish dictionary contained in the files in data/es/dictionary
     (this package) and share/es/dicc.src (after installation) is
     extracted from the Spanish Resource Grammar projects
     (http://www.upf.edu/pdi/iula/montserrat.marimon/srg.html) and was
     developed at the Institut Universitari de Linguistica Aplicada
     (http://www.iula.upf.edu) of the Universitat Pompeu Fabra. It is
     distributed under a "Lesser General Public License For Linguistic
     Resources" license. You'll find the license in the file
     LICENSES/LGPLLR.license included in this package or at
     http://sanskrit.inria.fr/DATA/LGPLLR.html

10.- Ancient Spanish dictionary was developed at the Universitat
     Pompeu Fabra in Barcelona by Cristina S�nchez Marco. This
     dictionary is a largely adapted and extended version of the
     Spanish Resource Grammar dictionary described above. The ancient
     Spanish dictionary is distributed under Lesser General Public
     License For Linguistic Resources. You'll find the license in the
     file LICENSES/LGPLLR.license included in this package or at
     http://sanskrit.inria.fr/DATA/LGPLLR.html

11.- Catalan dictionary conatined the files in data/ca/dictionary
     (this package) and share/ca/dicc.src (after installation) was
     obtained from "El Corrector", a Catalan spellchecker project
     (http://projectes.lafarga.cat/projects/corrector), which was
     funded by the Catalan Government, developed by Fundacio Barcelona
     Media (http://www.barcelonamedia.org/), and released under GPL.

12.- Portuguese dictionary contained in the files in
     data/pt/dictionary (this package) and share/pt/dicc.src (after
     installation) was extracted from the LABEL-LEX project
     (http://label.ist.utl.pt/pt/labellex_pt.php) developed by
     Laborat�rio de Engenharia da Linguagem (http://label.ist.utl.pt)
     at Universidade T�cnica de Lisboa (http://www.utl.pt), and
     adapted to FreeLing by Pablo Gamallo and Marcos Garcia, from
     Universidade de Santiago de Compostela.  It is distributed under
     its original GPLv3 license.

13.- Welsh dictionary contained in the files in data/cy/dictionary
     (this package) and share/cy/dicc.src (after installation) was
     extracted from Eurfa project (http://kevindonnelly.org.uk/eurfa/)
     developed by Kevin Donelly. It is distributed under its original
     GPL license.

14.- Asturian dictionary contained in the files in data/as/dictionary
     (this package) and share/as/dicc.src (after installation) was
     developed at the Universidad the Oviedo in the framework of the
     Eslema project (http://www.uniovi.es/eslema/). It is distributed
     under its original GPL license.

15.- French dictionary is extracted from LEFFF (Lexique des Formes Fl�chies du Fran�ais), 
     developed by Beno�t Sagot at INRIA / Universit� de Paris 7 
     See http://alpage.inria.fr/~sagot/lefff.html for details.
     These data are distributed under their original LGPL-LR license

10.- Ancient Spanish dictionary was developed at the Universitat
     Pompeu Fabra in Barcelona by Cristina S�nchez Marco. This
     dictionary is a largely adapted and extended version of the
     Spanish Resource Grammar dictionary described above. The ancient
     Spanish dictionary is distributed under Lesser General Public
     License For Linguistic Resources. You'll find the license in the
     file LICENSES/LGPLLR.license included in this package or at
     http://sanskrit.inria.fr/DATA/LGPLLR.html

11.- Catalan dictionary conatined the files in data/ca/dictionary
     (this package) and share/ca/dicc.src (after installation) was
     obtained from "El Corrector", a Catalan spellchecker project
     (http://projectes.lafarga.cat/projects/corrector), which was
     funded by the Catalan Government, developed by Fundacio Barcelona
     Media (http://www.barcelonamedia.org/), and released under GPL.

12.- Portuguese dictionary contained in the files in
     data/pt/dictionary (this package) and share/pt/dicc.src (after
     installation) was extracted from the LABEL-LEX project
     (http://label.ist.utl.pt/pt/labellex_pt.php) developed by
     Laborat�rio de Engenharia da Linguagem (http://label.ist.utl.pt)
     at Universidade T�cnica de Lisboa (http://www.utl.pt), and
     adapted to FreeLing by Pablo Gamallo and Marcos Garcia, from
     Universidade de Santiago de Compostela.  It is distributed under
     its original GPLv3 license.

13.- Welsh dictionary contained in the files in data/cy/dictionary
     (this package) and share/cy/dicc.src (after installation) was
     extracted from Eurfa project (http://kevindonnelly.org.uk/eurfa/)
     developed by Kevin Donelly. It is distributed under its original
     GPL license.

14.- Asturian dictionary contained in the files in data/as/dictionary
     (this package) and share/as/dicc.src (after installation) was
     developed at the Universidad the Oviedo in the framework of the
     Eslema project (http://www.uniovi.es/eslema/). It is distributed
     under its original GPL license.

15.- French dictionary is extracted from LEFFF (Lexique des Formes Fl�chies du Fran�ais), 
     developed by Beno�t Sagot at INRIA / Universit� de Paris 7 
     See http://alpage.inria.fr/~sagot/lefff.html for details.
     These data are distributed under their original 

10.- Ancient Spanish dictionary was developed at the Universitat
     Pompeu Fabra in Barcelona by Cristina S�nchez Marco. This
     dictionary is a largely adapted and extended version of the
     Spanish Resource Grammar dictionary described above. The ancient
     Spanish dictionary is distributed under Lesser General Public
     License For Linguistic Resources. You'll find the license in the
     file LICENSES/LGPLLR.license included in this package or at
     http://sanskrit.inria.fr/DATA/LGPLLR.html

11.- Catalan dictionary conatined the files in data/ca/dictionary
     (this package) and share/ca/dicc.src (after installation) was
     obtained from "El Corrector", a Catalan spellchecker project
     (http://projectes.lafarga.cat/projects/corrector), which was
     funded by the Catalan Government, developed by Fundacio Barcelona
     Media (http://www.barcelonamedia.org/), and released under GPL.

12.- Portuguese dictionary contained in the files in
     data/pt/dictionary (this package) and share/pt/dicc.src (after
     installation) was extracted from the LABEL-LEX project
     (http://label.ist.utl.pt/pt/labellex_pt.php) developed by
     Laborat�rio de Engenharia da Linguagem (http://label.ist.utl.pt)
     at Universidade T�cnica de Lisboa (http://www.utl.pt), and
     adapted to FreeLing by Pablo Gamallo and Marcos Garcia, from
     Universidade de Santiago de Compostela.  It is distributed under
     its original GPLv3 license.

13.- Welsh dictionary contained in the files in data/cy/dictionary
     (this package) and share/cy/dicc.src (after installation) was
     extracted from Eurfa project (http://kevindonnelly.org.uk/eurfa/)
     developed by Kevin Donelly. It is distributed under its original
     GPL license.

14.- Asturian dictionary contained in the files in data/as/dictionary
     (this package) and share/as/dicc.src (after installation) was
     developed at the Universidad the Oviedo in the framework of the
     Eslema project (http://www.uniovi.es/eslema/). It is distributed
     under its original GPL license.

15.- French dictionary is extracted from LEFFF (Lexique des Formes
     Fl�chies du Fran�ais) developed by Beno�t Sagot at INRIA-
     Universit� de Paris 7. See http://alpage.inria.fr/~sagot/lefff.html 
     for details. These data are distributed under their original
     Lesser General Public License For Linguistic Resources (LGPL-LR).
     You'll find the license in the file LICENSES/LGPLLR.license
     included in this package or at http://sanskrit.inria.fr/DATA/LGPLLR.html

16.- FOMA code under src/libfreeling/foma is copyrighted by Mans Hulden,
     and distributed under GPL. See http://code.google.com/p/foma/ for details.

17.- All other data and code modules in this package are distributed
     under GNU General Public License (GPLv3) and copyrighted by
     Universitat Polit�cnica de Catalunya.

     You can find a copy of GPL license in the file
     LICENSES/GPL.license included in this package, or at
     http://www.gnu.org/licenses/gpl
