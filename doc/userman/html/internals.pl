# LaTeX2HTML 2008 (1.71)
# Associate internals original text with physical files.


$key = q/file-hmm/;
$ref_files{$key} = "$dir".q|node49.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_brants00/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-dates/;
$ref_files{$key} = "$dir".q|node31.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_vossen98a/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/chap-modules/;
$ref_files{$key} = "$dir".q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/t-langs/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/file-alter/;
$ref_files{$key} = "$dir".q|node45.html|; 
$noresave{$key} = "$nosave";

$key = q/ssec-data/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-requirements/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/mod-coref/;
$ref_files{$key} = "$dir".q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-installation/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-maco/;
$ref_files{$key} = "$dir".q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/c-adding-lang/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/file-sense/;
$ref_files{$key} = "$dir".q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_fellbaum98/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_atserias05/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-tagset/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_padro98a/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/semdb/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_carreras03/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-prob/;
$ref_files{$key} = "$dir".q|node43.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-foma/;
$ref_files{$key} = "$dir".q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-pattern/;
$ref_files{$key} = "$dir".q|node78.html|; 
$noresave{$key} = "$nosave";

$key = q/file-split/;
$ref_files{$key} = "$dir".q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-tgz/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-pos/;
$ref_files{$key} = "$dir".q|node48.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-deb/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/file-dep/;
$ref_files{$key} = "$dir".q|node60.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-install-svn/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/file-wn/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/tr-tags/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/file-tok/;
$ref_files{$key} = "$dir".q|node21.html|; 
$noresave{$key} = "$nosave";

$key = q/file-suf/;
$ref_files{$key} = "$dir".q|node34.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-custom/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/mod-ukb/;
$ref_files{$key} = "$dir".q|node47.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_karlsson95/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-phon/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/file-mw/;
$ref_files{$key} = "$dir".q|node36.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-cond/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/file-relax/;
$ref_files{$key} = "$dir".q|node50.html|; 
$noresave{$key} = "$nosave";

$key = q/wnmap/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/file-usermap/;
$ref_files{$key} = "$dir".q|node29.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fex-rules/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_soon01/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/ss-options/;
$ref_files{$key} = "$dir".q|node93.html|; 
$noresave{$key} = "$nosave";

$key = q/fex-file/;
$ref_files{$key} = "$dir".q|node76.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-rgf/;
$ref_files{$key} = "$dir".q|node75.html|; 
$noresave{$key} = "$nosave";

$key = q/file-cfg/;
$ref_files{$key} = "$dir".q|node58.html|; 
$noresave{$key} = "$nosave";

$key = q/file-nec/;
$ref_files{$key} = "$dir".q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chang11/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/file-ner/;
$ref_files{$key} = "$dir".q|node38.html|; 
$noresave{$key} = "$nosave";

$key = q/lang-ident/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_agirre09/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/mod-sense/;
$ref_files{$key} = "$dir".q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/file-quant/;
$ref_files{$key} = "$dir".q|node41.html|; 
$noresave{$key} = "$nosave";

$key = q/file-dict/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/ss-config/;
$ref_files{$key} = "$dir".q|node94.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-execute/;
$ref_files{$key} = "$dir".q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/sec-main/;
$ref_files{$key} = "$dir".q|node85.html|; 
$noresave{$key} = "$nosave";

$key = q/cap-analyzer/;
$ref_files{$key} = "$dir".q|node86.html|; 
$noresave{$key} = "$nosave";

$key = q/file-numb/;
$ref_files{$key} = "$dir".q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/file-punt/;
$ref_files{$key} = "$dir".q|node27.html|; 
$noresave{$key} = "$nosave";

1;

